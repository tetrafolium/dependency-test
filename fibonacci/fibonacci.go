package fibonacci

// fibonacci is a function that returns
// a function that returns an int.
func Fibonacci() func() int {
	x, y := 0, 1
	return func() int {
		x, y = y, x + y
		return x
	}
} 